# Richtlinien-Umsetzung NeuVector
Stand 02.10.2024 sind nicht alle Policies direkt mit NeuVector umsetzbar. NeuVector kann nur kann mit seinem Admission Control nur auf ``deployments`` zugreifen, somit können Policies wie z.B. ``011-require-pvc-accessmode`` nicht umgesetzt werden.

Auch ist es nur möglich ALLE Policies zu importieren und nicht einzelne. Somit müsste man zum Importieren von weiteren Policies die bestehenden erst exportieren, dann die Dateien lokal zusammenführen und dann wieder importieren.

Wir als IG BvC - PoC II AP 5.2 werden erstmal keine weitere Entwicklung in NeuVector investieren sondern fokusieren uns auf Kyverno und Validating Admission Policy.